import { AppRegistry } from 'react-native';
import App from './App';
import { StackNavigator,} from 'react-navigation';
 import HomeScreen from './Screens/Home';
 import MainScreen from './Screens/MainScreen';
 import SignInScreen from './Screens/SignInScreen';
 import ChatScreen from './Screens/ChatScreen';
 import ContactScreen from './Screens/ContactScreen'
 import MessageScreen from './Screens/MessageScreen';
 import ProfileScreen from './Screens/ProfileScreen';
 import UserProfileScreen from './Screens/UserProfileScreen';
import UsersListScreen from './Screens/UsersListScreen';

const ChitChat = StackNavigator({
    App:{screen : App},
     Home: { screen: HomeScreen },
     SignIn: { screen: SignInScreen },
     Chats: {screen: ChatScreen},
     Main :{screen : MainScreen},
     Message:{screen:MessageScreen},
     profile:{screen: ProfileScreen},
     uprofile:{screen: UserProfileScreen},
     contact:{screen: ContactScreen},
     userslist :{screen : UsersListScreen}
   }); 
   AppRegistry.registerComponent('ChitChat', () => ChitChat);


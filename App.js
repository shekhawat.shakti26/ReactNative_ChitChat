
import React, { Component } from 'react';
import {  AppRegistry, StyleSheet, Navigator, Image,
   NativeModules,DeviceEventEmitter,BackHandler,ToastAndroid} from 'react-native';
import { Container,} from 'native-base';
import { StackNavigator,} from 'react-navigation';
import firebaseApp from './Screens/Firebase';

export default class App extends Component {
  static navigationOptions = {
    title: 'WELCOME',
    header:null, 
  }; 
  constructor(props) {
   super(props);
   this.state={
     count:0
   }
   }
 componentDidMount() { // on page load function to check if user is logged In or not
  
   const { navigate } = this.props.navigation;
   firebaseApp.auth().onAuthStateChanged(function(user) {
     if (user) {
       // User is signed in.
      navigate('Main');
     }
     else {
       //user is not signed in navigate to login page
      navigate('Home');
     }
   });
 } 
 
  render() {
    const { navigate } = this.props.navigation;
    return (
    <Container style={styles.Container}>
        <Image style={styles.stretch} source={require('./splash.png')} />

    </Container>
    
    );
  }
 }
 var styles = StyleSheet.create({
  Container :{
   alignItems:'center'
  },
  stretch: {
   width: '100%',
   height: '100%'
 }
 });
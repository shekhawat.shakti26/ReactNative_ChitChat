# chitchat-react-native
**This is react native chatting app**.

### Follow the steps to run the project:
1. Clone the repo in your local storage.
2. Use **npm install** for installing required modules.
3. Connect your android device to the system in debug mode. 
4. run **react-native run-android** to install the app on device.' **OR** if you are on *linux*       then use **npm run android-linux**
5. ***Enjoy the ChitChat*** .

### NOTE
If got error **React.Proptype.number** OR **React.Proptype.string undefined"**
Then  In **node_modules/react-native-parallax-view/lib/parallexView.js** 
Add  **const PropTypes = require('prop-types');**    
**const createReactClass = require('create-react-class');**

Replace all **React.PropTypes** with **PropTypes** And
             **React.createClass** with **createReactClass**
             
REFERENCE : https://github.com/lelandrichardson/react-native-parallax-view/issues/57
